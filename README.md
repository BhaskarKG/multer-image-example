# React Features

* NPM commands
  * `npm run buildDev` - to build bundle files
  * `npm run startDev` - to express server

* Use `http://localhost:9001/imageUploadMethodOne` - to access image upload page
* Main Component of above image upload route - src/pages/image-upload-method-one/ImageUploadMethodOne.js
